-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 16/05/2020 às 13:56
-- Versão do servidor: 5.6.41-84.1
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lucia980_desafio`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Categoria`
--

CREATE TABLE `Categoria` (
  `id` int(4) NOT NULL,
  `codigo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nome` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `Categoria`
--

INSERT INTO `Categoria` (`id`, `codigo`, `nome`) VALUES
(16, 'FFFJJJJ3333 ', 'outra categoria x'),
(3, 'CCC ', 'C Q SABE'),
(18, '222 ', 'otraaaaa'),
(5, 'FFF g', 'C '),
(7, '333', 'PrimeiroNome'),
(8, '444', 'SegundoNome'),
(9, '444', 'SegundoNome'),
(10, '444', 'SegundoNome'),
(11, '5555', 'SegundoNome'),
(15, 'X12399SP', 'Categoria Legal '),
(14, '114', 'dsddddfdf'),
(17, NULL, NULL),
(19, '56', 'PrimeiroNome');

-- --------------------------------------------------------

--
-- Estrutura para tabela `Produto`
--

CREATE TABLE `Produto` (
  `idProduto` int(4) NOT NULL,
  `nome` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preco` decimal(4,0) DEFAULT NULL,
  `descricao` text COLLATE utf8_unicode_ci,
  `quantidade` int(4) DEFAULT NULL,
  `categoria` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `Produto`
--

INSERT INTO `Produto` (`idProduto`, `nome`, `codigo`, `preco`, `descricao`, `quantidade`, `categoria`, `foto`) VALUES
(29, 'fghij', '998877', '8', '       d', 3, 'Category 4', '160520200804282.jpg'),
(28, 'Mais um ', '998877', '9', 'd', 10, 'Category 4', '150520202245084.jpg'),
(27, 'teste', '9876', '775', 's', 1000, 'Category 2', '150520202243462.jpg'),
(26, 'teste', '12345', '9', 'ddd', 999, 'Category 4', '150520202241584.jpg'),
(25, 'Produto Dois', '9876', '5', 'a', 500, 'Category 2', '150520202234558.jpg'),
(24, 'Mais um ', '9876s', '775', 'ssss', 500, 'Category 2', '150520202233221.jpg'),
(23, 'Mais um ', '998877', '9', 'aaaaa', 20, 'Category 4', '150520202232401.jpg'),
(22, 'teste', '12345', '9', 'hhhh', 899, 'Category 4', '150520202223511.jpg'),
(21, 'teste', '12345', '9', 'ww', 1000, 'Category 1', '150520202222125.jpg'),
(20, 'Produto Novo', '12345', '3', 'outro', 200, 'Category 1', '150520202220452.jpg'),
(17, 'teste', '998877', '9', 'fffff', 999, 'Category 1', '150520202149193.jpg'),
(18, 'teste', '998877', '9', 'yyyy', 899, 'Category 4', '150520202159557.jpg'),
(19, 'Produto Dois', '12345', '775', '    llllll2', 500, NULL, '150520202212387.jpg');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `Categoria`
--
ALTER TABLE `Categoria`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `Produto`
--
ALTER TABLE `Produto`
  ADD KEY `idProduto` (`idProduto`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `Categoria`
--
ALTER TABLE `Categoria`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `Produto`
--
ALTER TABLE `Produto`
  MODIFY `idProduto` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
